﻿namespace OKE.Reservation_Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reservations",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        ReservedRoom_Id = c.Guid(),
                        ReservingEmployee_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rooms", t => t.ReservedRoom_Id)
                .ForeignKey("dbo.Employees", t => t.ReservingEmployee_Id)
                .Index(t => t.ReservedRoom_Id)
                .Index(t => t.ReservingEmployee_Id);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservations", "ReservingEmployee_Id", "dbo.Employees");
            DropForeignKey("dbo.Reservations", "ReservedRoom_Id", "dbo.Rooms");
            DropIndex("dbo.Reservations", new[] { "ReservingEmployee_Id" });
            DropIndex("dbo.Reservations", new[] { "ReservedRoom_Id" });
            DropTable("dbo.Rooms");
            DropTable("dbo.Reservations");
            DropTable("dbo.Employees");
        }
    }
}
