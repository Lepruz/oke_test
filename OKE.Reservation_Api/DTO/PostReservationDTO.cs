﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OKE.Reservation_Api.DTO
{
    public class PostReservationDTO
    {
        public long Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int EmployeeId  { get; set; }
        public Guid RoomId { get; set; }
    }
}