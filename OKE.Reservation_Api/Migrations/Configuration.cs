﻿namespace OKE.Reservation_Api.Migrations
{
    using OKE.Reservation_Api.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OKE.Reservation_Api.DAL.ReservationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OKE.Reservation_Api.DAL.ReservationContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Employees.AddOrUpdate(e => e.Id,
                new Employee() { Id = 1, Name = "Jan", Surname = "Nowak" },
               new Employee() { Id = 2, Name = "Adam", Surname = "Kowalski" },
               new Employee() { Id = 3, Name = "Piotr", Surname = "Michalski" },
               new Employee() { Id = 4, Name = "Maciej", Surname = "Piotrowski" });
;

            context.Rooms.AddOrUpdate(r => r.Id, 
                new Room() { Id = Guid.NewGuid(), Name = "Conference room" },
                new Room() { Id = Guid.NewGuid(), Name = "Managment room" },
                new Room() { Id = Guid.NewGuid(), Name = "Chill room" }
                );
            
        }
    }
}
