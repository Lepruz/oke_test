﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OKE.Reservation_Api.Entities
{
    public class Reservation
    {
        [Key]
        public long Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public Employee ReservingEmployee { get; set; }
        public Room ReservedRoom { get; set; }
    }
}