﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OKE.Reservation_Api.DTO
{
    public class GetReservationDTO
    {
        public long Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Employee { get; set; }
        public string RoomName { get; set; }
    }
}