﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using OKE.Reservation_Api.DAL;
using OKE.Reservation_Api.DTO;
using OKE.Reservation_Api.Entities;

namespace OKE.Reservation_Api.Controllers
{
    public class ReservationsController : ApiController
    {
        private ReservationContext _ctx { get; set; }


        public ReservationsController(ReservationContext ctx)
        {
            _ctx = ctx;
        }

        //api/reservations/
        [HttpGet]
        public IEnumerable<GetReservationDTO> GetAllReservations()
        {
            return _ctx.Reservations.Include(x => x.ReservedRoom).Include(x => x.ReservingEmployee).AsEnumerable().Select(x => new GetReservationDTO()
            {
                Id = x.Id,
                Employee = $"{x.ReservingEmployee.Name} {x.ReservingEmployee.Surname}",
                RoomName = x.ReservedRoom.Name,
                StartDateTime = x.StartDateTime,
                EndDateTime = x.EndDateTime
            });
        }

        [Route("api/Reservations/GetReservationsForEmployee")]
        [HttpGet]
        public async Task<IHttpActionResult> GetReservationsForEmployee([FromUri] int id)
        {
            if (id == null)
            {
                return BadRequest("Please provide employee id");
            }
            var reservations = _ctx.Reservations.Include(x => x.ReservedRoom).Include(x => x.ReservingEmployee).Where(x => x.ReservingEmployee.Id == id)
                .AsEnumerable()
                .Select(x => new GetReservationDTO()
                {
                    Id = x.Id,
                    Employee = $"{x.ReservingEmployee.Name} {x.ReservingEmployee.Surname}",
                    RoomName = x.ReservedRoom.Name,
                    StartDateTime = x.StartDateTime,
                    EndDateTime = x.EndDateTime
                });

            return Ok(reservations);
        }

        [Route("api/Reservations/GetReservationsForRoom")]
        [HttpGet]
        public async Task<IHttpActionResult> GetReservationsForRoom([FromUri] Guid id)
        {
            if (id == null)
            {
                return BadRequest("Please provide room id");
            }
            var reservations = _ctx.Reservations.Include(x => x.ReservedRoom).Include(x => x.ReservingEmployee).Where(x => x.ReservedRoom.Id.Equals(id))
                .AsEnumerable()
                .Select(x => new GetReservationDTO()
                {
                    Id = x.Id,
                    Employee = $"{x.ReservingEmployee.Name} {x.ReservingEmployee.Surname}",
                    RoomName = x.ReservedRoom.Name,
                    StartDateTime = x.StartDateTime,
                    EndDateTime = x.EndDateTime
                });

            return Ok(reservations);
        }

        //api/reservations/
        [HttpPost]
        public async Task<IHttpActionResult> PostReservation(PostReservationDTO reservationDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var employee = await _ctx.Employees.FindAsync(reservationDto.EmployeeId) ?? throw new Exception("Invalid employee id");
                var room = await _ctx.Rooms.FindAsync(reservationDto.RoomId) ?? throw new Exception("Invalid room id");

                var overlappedReservation = _ctx.Reservations.Include(x => x.ReservedRoom)
                    .Where(r => DateTime.Compare(reservationDto.StartDateTime, r.EndDateTime) < 0
                        && DateTime.Compare(r.StartDateTime, reservationDto.EndDateTime) < 0 && r.ReservedRoom.Id.Equals(reservationDto.RoomId))
                    .FirstOrDefault();

                if (overlappedReservation != null)
                {
                    throw new Exception($"Room is already reserved at this time! Overlapped reservation Id: {overlappedReservation.Id}");
                }

                var reservation = new Reservation()
                {
                    StartDateTime = reservationDto.StartDateTime,
                    EndDateTime = reservationDto.EndDateTime,
                    ReservingEmployee = employee,
                    ReservedRoom = room
                };

                _ctx.Reservations.Add(reservation);
                await _ctx.SaveChangesAsync();
                return Ok($"Your reservation Id: {reservation.Id}");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //api/reservations/{id}
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteReservation(long id)
        {
            Reservation reservation = await _ctx.Reservations.FindAsync(id);
            if (reservation == null)
            {
                return NotFound();
            }

            _ctx.Reservations.Remove(reservation);
            await _ctx.SaveChangesAsync();

            return Ok("Successfuly canceled reservation");
        }

        //api/Reservations/{id}
        [HttpPut]
        public async Task<IHttpActionResult> PutReservation(long id, Reservation reservation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reservation.Id)
            {
                return BadRequest();
            }

            _ctx.Entry(reservation).State = EntityState.Modified;

            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

    }
}