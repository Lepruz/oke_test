﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OKE.Reservation_Api.Entities
{
    public class Room
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}