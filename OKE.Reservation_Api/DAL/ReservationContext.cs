﻿using OKE.Reservation_Api.Entities;
using System.Data.Entity;


namespace OKE.Reservation_Api.DAL
{
    public class ReservationContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Room> Rooms { get; set; }
    }
}